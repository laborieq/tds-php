<?php
use App\Covoiturage\Lib\ConnexionUtilisateur;

/** @var ModeleUtilisateur $utilisateur */
echo '<p><strong>Login :</strong> ' . htmlspecialchars($utilisateur->getLogin()) . '</p>';
echo '<p><strong>Nom :</strong> ' . htmlspecialchars($utilisateur->getNom()) . '</p>';
echo '<p><strong>Prénom :</strong> ' . htmlspecialchars($utilisateur->getPrenom()) . '</p>';

$loginURL = rawurlencode($utilisateur->getLogin());

// Vérifie si l'utilisateur connecté est celui correspondant au login
if (ConnexionUtilisateur::estUtilisateur($utilisateur->getLogin())) {
    echo '<p>
            <a href="http://localhost/tds-php/TD8/web/controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginURL . '">Modifier</a>
            - 
            <a href="http://localhost/tds-php/TD8/web/controleurFrontal.php?controleur=utilisateur&action=supprimer&login=' . $loginURL . '">Supprimer</a>
          </p>';
}