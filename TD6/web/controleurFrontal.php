<?php
use App\TD6\Covoiturage\Controleur\ControleurUtilisateur;
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

$chargeurDeClasse = new App\TD6\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
$chargeurDeClasse->addNamespace('App\TD6\Covoiturage', __DIR__ . '/../src');

$controleur = $_GET["controleur"] ?? 'utilisateur';

$nomDeClasseControleur = "App\\Covoiturage\\Controleur\\Controleur" . ucfirst($controleur);

if (class_exists($nomDeClasseControleur)) {
    if (isset($_GET["action"])) {
        $action = $_GET["action"];
        $methodesDisponibles = get_class_methods($nomDeClasseControleur);

        if (in_array($action, $methodesDisponibles)) {
            $nomDeClasseControleur::$action();
        } else {
            ControleurUtilisateur::afficherErreur("Action non reconnue : $action");
        }
    } else {
        $nomDeClasseControleur::afficherListe();
    }
} else {
    ControleurUtilisateur::afficherErreur("Contrôleur non reconnu : $controleur");
}