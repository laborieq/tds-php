<?php
use App\Covoiturage\Lib\ConnexionUtilisateur;
if (ConnexionUtilisateur::estConnecte()){
    $hideNonConnecter = "hidden";
    $hideConnecter= "";
}else{
    $hideNonConnecter = "";
    $hideConnecter= "hidden";
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="../ressources/css/style.css">
        <title>
            <?php
            /**
             * @var string $titre
             */
            echo $titre;
            ?>
        </title>
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
                    </li>
                    <li>
                        <a href="controleurFrontal.php?action=afficherFormulairePreference">
                            <img src="../ressources/img/heart.png">
                        </a>
                    </li>

                    <li <?=$hideNonConnecter?> >
                        <a href="controleurFrontal.php?action=afficherFormulaireCreation&controleur=utilisateur"><img src="../ressources/img/adduser.png" ></a>
                    </li>
                    <li <?=$hideNonConnecter?>  >
                        <a href="controleurFrontal.php?action=afficherFormulaireConnexion&controleur=utilisateur"><img src="../ressources/img/enter.png" ></a>
                    </li>
                    <li <?=$hideConnecter?> >
                        <a href="controleurFrontal.php?action=afficherDetail&controleur=utilisateur&login=<?=rawurlencode(ConnexionUtilisateur::getLoginUtilisateurConnecte())?>"><img src="../ressources/img/user.png" ></a>
                    </li>
                    <li <?=$hideConnecter?>  >
                        <a href="controleurFrontal.php?action=deconnecter&controleur=utilisateur"><img src="../ressources/img/logout.png" ></a>
                    </li>

                </ul>
            </nav>
        </header>
        <main>
            <?php
            /**
             * @var string $cheminCorpsVue
             */
            require __DIR__ . "/{$cheminCorpsVue}";
            ?>
        </main>
        <footer>
            <p>
                Site de covoiturage de moi
            </p>
        </footer>
    </body>
</html>