<form method="get" action='controleurFrontal.php'>
    <input type='hidden' name='action' value='creerDepuisFormulaire'>
    <input type='hidden' name='controleur' value='utilisateur'>
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label> :
            <input class="InputAddOn-field" type="text" placeholder="artich" name="login" id="login_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom&#42;</label> :
            <input class="InputAddOn-field" type="text" placeholder="Tichault" name="nom" id="nom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom&#42;</label> :
            <input class="InputAddOn-field" type="text" placeholder="Richard" name="prenom" id="prenom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdpHache_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdpHache" id="mdpHache_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdpHache2_id">Vérification du mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdpHache2" id="mdpHache2_id" required>
        </p>
        <?php
        use App\Covoiturage\Lib\ConnexionUtilisateur;

        if (ConnexionUtilisateur::estAdministrateur()) : ?>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="estAdmin_id">Administrateur&#42;</label>
                <input class="InputAddOn-field" type="checkbox" name="estAdmin" id="estAdmin_id">
            </p>
        <?php endif; ?>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="email_id">Email&#42;</label>
            <input class="InputAddOn-field" type="email" value="" placeholder="artichaut@naheulbeuk.fr" name="email" id="email_id" required>
        </p>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>