<?php
namespace TD5\App\Covoiturage\Modele;
use TD5\App\Covoiturage\Modele\ConnexionBaseDeDonnees;

class ModeleUtilisateur {
    private string $login;
    private string $nom;
    private string $prenom;

    public function __construct(string $login, string $nom, string $prenom) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): ModeleUtilisateur {
        return new ModeleUtilisateur(
            $utilisateurFormatTableau['login'],
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom']
        );
    }

    public function getLogin(): string {
        return $this->login;
    }

    public function getNom(): string {
        return $this->nom;
    }

    public function getPrenom(): string {
        return $this->prenom;
    }

    public static function recupererUtilisateurs(): array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT * FROM utilisateur";
        $pdoStatement = $pdo->query($sql);
        $utilisateurs = [];

        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }

    public function ajouter() : void {
        $sql = "INSERT INTO utilisateur VALUES (:newLoginTag, :newNomTag, :newPrenomTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "newLoginTag" => $this->login,
            "newNomTag" => $this->nom,
            "newPrenomTag" => $this->prenom,
        );
        $pdoStatement->execute($values);
    }

    public static function recupererUtilisateurParLogin(string $login) : ?ModeleUtilisateur {
        $sql = "SELECT * FROM utilisateur WHERE login = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
        );
        $pdoStatement->execute($values);
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if ($utilisateurFormatTableau === false) {
            return null;
        }

        return ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }
}
?>