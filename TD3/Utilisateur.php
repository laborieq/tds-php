<?php
require_once "ConnexionBaseDeDonnees.php";

class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    public function __construct(string $nom, string $prenom, string $login) {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->login = substr($login, 0, 64);
    }

    public function __toString() {
        return "Nom : $this->nom, Prénom : $this->prenom, Login : $this->login";
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur {
        return new Utilisateur(
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom'],
            $utilisateurFormatTableau['login']
        );
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public static function recupererUtilisateurs(): array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT * FROM utilisateur";
        $pdoStatement = $pdo->query($sql);
        $utilisateurs = [];

        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }

    public static function recupererUtilisateurParLogin(string $login) : ?Utilisateur {
        $sql = "SELECT * FROM utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas d'utilisateur correspondant
        $utilisateurFormatTableau = $pdoStatement->fetch();

        if ($utilisateurFormatTableau === false) {
            return null;
        }

        return Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }

    public function ajouter() : void {
        $sql = "INSERT INTO utilisateur VALUES (:newLoginTag, :newNomTag, :newPrenomTag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "newLoginTag" => $this->login,
            "newNomTag" => $this->nom,
            "newPrenomTag" => $this->prenom,
        );
        $pdoStatement->execute($values);
    }
}
?>