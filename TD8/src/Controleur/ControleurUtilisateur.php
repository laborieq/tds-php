<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique {
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository)->recuperer();
        $titre = "Liste des utilisateurs";
        ControleurGenerique::afficherVue('../vue/vueGenerale.php', [
                "utilisateurs" => $utilisateurs,
                "titre" => $titre,
                "cheminCorpsVue" => "utilisateur/liste.php"
        ]);
    }

    public static function afficherDetail() : void {
        $login = $_GET["login"];
        if ($login && (new UtilisateurRepository())->recupererParClePrimaire($login)) {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            ControleurGenerique::afficherVue('../vue/vueGenerale.php', [
                "utilisateur" => $utilisateur,
                "titre" => "Détail utilisateur",
                "cheminCorpsVue" => "utilisateur/detail.php"
            ]);
        } else {
            ControleurGenerique::afficherErreur("utilisateur inexistant");
        }
    }

    public static function afficherFormulaireCreation() : void {
        ControleurGenerique::afficherVue('../vue/vueGenerale.php', [
            "titre" => "Creation utilisateur",
            "cheminCorpsVue" => "utilisateur/formulaireCreation.php"
        ]);
    }

    public static function creerDepuisFormulaire() : void {
        if ($_GET["mdpHache"] === $_GET["mdpHache2"]) {
            $estAdminForm = isset($_GET["estAdmin"]);
            $estAdminFinal = ConnexionUtilisateur::estAdministrateur() ? $estAdminForm : false;

            $utilisateur = self::construireDepuisFormulaire([
                "login" => $_GET["login"],
                "nom" => $_GET["nom"],
                "prenom" => $_GET["prenom"],
                "mdpHache" => $_GET["mdpHache"],
                "estAdmin" => $estAdminFinal,
                "email" => "",
                "emailAValider" => $_GET["email"],
                "nonce" => mm; //à modifier
            ]);

            (new UtilisateurRepository)->ajouter($utilisateur);

            $utilisateurs = (new UtilisateurRepository)->recuperer();
            self::afficherVue('../vue/vueGenerale.php', [
                "titre" => "Liste des utilisateurs",
                "cheminCorpsVue" => "utilisateur/utilisateurCree.php",
                "utilisateurs" => $utilisateurs
            ]);
        } else {
            self::afficherErreur("Mots de passe distincts");
        }
    }

    public static function supprimer(): void {
        if (!isset($_GET["login"])) {
            ControleurGenerique::afficherErreur("Login manquant.");
            return;
        }

        $login = $_GET["login"];

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur === null) {
            ControleurGenerique::afficherErreur("Utilisateur non trouvé.");
            return;
        }

        if (!ConnexionUtilisateur::estUtilisateur($login)) {
            ControleurGenerique::afficherErreur("Vous ne pouvez supprimer que votre propre compte.");
            return;
        }

        (new UtilisateurRepository())->supprimer($login);

        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ControleurGenerique::afficherVue("../vue/vueGenerale.php", [
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php",
            "login" => $login,
            "utilisateurs" => $utilisateurs
        ]);
    }

    public static function afficherFormulaireMiseAJour(): void {
        if (!isset($_GET["login"])) {
            ControleurGenerique::afficherErreur("Login manquant");
            return;
        }

        $login = $_GET["login"];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if ($utilisateur === null) {
            if (ConnexionUtilisateur::estAdministrateur()) {
                ControleurGenerique::afficherErreur("Login inconnu.");
            } else {
                ControleurGenerique::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
            }
            return;
        }

        if (!ConnexionUtilisateur::estUtilisateur($login) && !ConnexionUtilisateur::estAdministrateur()) {
            ControleurGenerique::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
            return;
        }

        ControleurGenerique::afficherVue("../vue/vueGenerale.php", [
            "titre" => "Formulaire de mise à jour",
            "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php",
            "utilisateur" => $utilisateur,
            "estAdmin" => ConnexionUtilisateur::estAdministrateur()
        ]);
    }

    public static function mettreAJour(): void {
        if (!isset($_GET["login"], $_GET["nom"], $_GET["prenom"], $_GET["old_mdpHache"], $_GET["new_mdpHache"], $_GET["new_mdpHache2"])) {
            ControleurGenerique::afficherErreur("Tous les champs du formulaire doivent être remplis.");
            return;
        }

        $login = $_GET["login"];
        $nom = $_GET["nom"];
        $prenom = $_GET["prenom"];
        $oldMdpHache = $_GET["old_mdpHache"];
        $newMdpHache = $_GET["new_mdpHache"];
        $newMdpHache2 = $_GET["new_mdpHache2"];

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if ($utilisateur == null) {
            if (ConnexionUtilisateur::estAdministrateur()) {
                ControleurGenerique::afficherErreur("Login inconnu.");
            } else {
                ControleurGenerique::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
            }
            return;
        }

        if (!ConnexionUtilisateur::estUtilisateur($login) && !ConnexionUtilisateur::estAdministrateur()) {
            ControleurGenerique::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté.");
            return;
        }

        if ($newMdpHache != $newMdpHache2) {
            ControleurGenerique::afficherErreur("Les nouveaux mots de passe ne correspondent pas.");
            return;
        }

        if (!ConnexionUtilisateur::estAdministrateur()) {
            if (!MotDePasse::verifier($oldMdpHache, $utilisateur->getMdpHache())) {
                ControleurGenerique::afficherErreur("L'ancien mot de passe est incorrect.");
                return;
            }
        }

        $utilisateur->setNom($nom);
        $utilisateur->setPrenom($prenom);

        $utilisateur->setMdpHache(MotDePasse::hacher($newMdpHache));

        if (ConnexionUtilisateur::estAdministrateur()) {
            $estAdmin = isset($_GET["estAdmin"]);
            $utilisateur->setEstAdmin($estAdmin);
        }

        (new UtilisateurRepository())->mettreAJour($utilisateur);

        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ControleurGenerique::afficherVue("../vue/vueGenerale.php", [
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php",
            "utilisateurs" => $utilisateurs,
            "login" => $login
        ]);
    }

    /**
     * @return Utilisateur
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur {
        $nonce = MotDePasse::genererChaineAleatoire();
        return new Utilisateur(
            $tableauDonneesFormulaire["login"],
            $tableauDonneesFormulaire["nom"],
            $tableauDonneesFormulaire["prenom"],
            MotDePasse::hacher($tableauDonneesFormulaire["mdpHache"]),
            $tableauDonneesFormulaire["estAdmin"],
            "",
            $tableauDonneesFormulaire["email"],
            $nonce
        );
    }

    public static function afficherFormulaireConnexion(): void {
        self::afficherVue("../vue/vueGenerale.php", ["titre"=>"Connexion", "cheminCorpsVue"=>"utilisateur/formulaireConnexion.php"]);
    }

    public static function connecter() : void {
        if (isset($_GET["login"]) && isset($_GET["mdp"])){
            $login = $_GET["login"];
            $mdp = $_GET["mdp"];
            /** @var Utilisateur $utilistateur */
            $utilistateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            if (MotDePasse::verifier($mdp, $utilistateur->getMdpHache())) {
                ConnexionUtilisateur::connecter($login);
                self::afficherVue("../vue/vueGenerale.php", ["titre"=>"Détail Utilisateur", "cheminCorpsVue" => "utilisateur/utilisateurConnecte.php", "login"=>$login, "utilisateur"=>$utilistateur]);
            } else {
                self::afficherErreur("Login et/ou mot de passe incorrect");
            }
        }else {
            self::afficherErreur("Login et/ou mot de passe manquant");
        }
    }

    public static function deconnecter() : void {
        ConnexionUtilisateur::deconnecter();
        $utilisateurs = (new UtilisateurRepository)->recuperer();
        self::afficherVue("../vue/vueGenerale.php", ["titre"=>"Liste des utilisateurs", "cheminCorpsVue"=>"utilisateur/utilisateurDeconnecte.php", "utilisateurs"=>$utilisateurs]);
    }
}