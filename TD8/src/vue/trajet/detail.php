<?php
/** @var Trajet $trajet */
$date = htmlspecialchars($trajet->getDate()->format("d/m/Y"));
$depart = htmlspecialchars($trajet->getDepart());
$idHTML = htmlspecialchars($trajet->getId());
$arrivee = htmlspecialchars($trajet->getArrivee());
$conducteurNom = htmlspecialchars($trajet->getConducteur()->getNom());
$conducteurPrenom = htmlspecialchars($trajet->getConducteur()->getPrenom());
$nonFumeur = $trajet->isNonFumeur() ? " non fumeur" : " ";

echo "<p>
            Le trajet $idHTML du $date partira de $depart pour aller à $arrivee (conducteur: $conducteurNom $conducteurPrenom).
        </p>";
?>