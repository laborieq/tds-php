<?php
use TD5\App\Covoiturage\Controleur\ControleurUtilisateur;
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';


// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new TD5\App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('TD5\App\Covoiturage', __DIR__ . '/../src');

$action = $_GET["action"];
ControleurUtilisateur::$action();
?>