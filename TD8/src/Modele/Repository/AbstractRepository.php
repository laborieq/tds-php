<?php
namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;

abstract class AbstractRepository {
    public function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject {
        $sql = "SELECT * from ". $this->getNomTable() ." WHERE ". $this->getNomClePrimaire()." = :clePrimaire";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array(
            "clePrimaire" => $clePrimaire,
        );
        $pdoStatement->execute($values);
        $objectFormatTableau = $pdoStatement->fetch();
        if ($objectFormatTableau) {
            return $this->construireDepuisTableauSQL($objectFormatTableau);
        } else {
            return null;
        }
    }

    protected abstract function getNomTable() : string;

    /** @return AbstractDataObject [] */
    public function recuperer() : array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = 'Select * From ' . $this->getNomTable();
        $pdoStatement = $pdo->query($sql);
        foreach ($pdoStatement as $users){
            $Tableau[] = $this->construireDepuisTableauSQL($users);
        }
        return $Tableau;
    }

    public function supprimer(string $valeurClePrimaire) : bool {
        $sql = "Delete from ".$this->getNomTable()." where ".$this->getNomClePrimaire()." = :clePrimaire";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = array("clePrimaire" => $valeurClePrimaire);
        return $pdoStatement->execute($values);
    }

    public function ajouter(AbstractDataObject $objet) : bool {
        $sql = "Insert into ".$this->getNomTable()." (".join(', ', $this->getNomsColonnes()).") 
        VALUES (:".join('Tag, :',$this->getNomsColonnes())."Tag)";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $values = $this->formatTableauSQL($objet);
        return $pdoStatement->execute($values);
    }

    public function mettreAJour(AbstractDataObject $object) : void{
        $colonnes = $this->getNomsColonnes();
        $values = $this->formatTableauSQL($object);
        $texte = "";
        for ($x =0; $x< count($colonnes)-1; $x = $x+1){
            $texte = $texte . " $colonnes[$x] = :$colonnes[$x]Tag,";
        }
        $texte = $texte . " $colonnes[$x] = :$colonnes[$x]Tag";

        $sql = "UPDATE " . $this->getNomTable() . " SET $texte WHERE $colonnes[0] = :$colonnes[0]Tag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute($values);
    }

    protected abstract function construireDepuisTableauSQL(array $objectFormatTableau) : AbstractDataObject;

    protected abstract function getNomClePrimaire() : string;

    /** @return string[] */
    protected abstract function getNomsColonnes() : array;

    protected abstract function formatTableauSQL(AbstractDataObject $object) : array;
}