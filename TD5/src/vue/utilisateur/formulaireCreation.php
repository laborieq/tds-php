<form method="get" action="controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : artich" name="login" id="login_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="last_name_id">Nom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Tichault" name="last_name" id="last_name_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="first_name_id">Prenom&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="Ex : Richard" name="first_name" id="first_name_id" required>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
    <input type='hidden' name='action' value='creerDepuisFormulaire'>
</form>