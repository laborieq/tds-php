<?php
namespace App\TD7\Covoiturage\Configuration;

class ConfigurationSite {
    static private int $dureeExpirationSession = 1800; //30 min.

    static public function getDureeExpirationSession(): int {
        return self::$dureeExpirationSession;
    }
}
?>