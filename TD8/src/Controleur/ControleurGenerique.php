<?php
namespace App\Covoiturage\Controleur;
use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique {
    protected static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . "/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherErreur(string $messageErreur = "") : void {
        if ($messageErreur == "") {
            $messageErreur = "Une erreur est survenue.";
        }
        self::afficherVue("../vue/erreur.php", ["titre" => "Erreur", "messageErreur" => $messageErreur]);
    }

    public static function afficherFormulairePreference(): void {
        self::afficherVue("../vue/formulairePreference.php", ["titre" => "Préférences de contrôleur"]);
    }

    public static function enregistrerPreference() : void {
        $controleurDefaut = $_GET['controleur_defaut'] ?? 'utilisateur';
        PreferenceControleur::enregistrer($controleurDefaut);
        self::afficherVue('../vue/preferenceEnregistree.php', ["message" => "La préférence de contrôleur est enregistrée !"]);
    }
}