<?php
namespace TD5\App\Covoiturage\Controleur;
use TD5\App\Covoiturage\Modele\ModeleUtilisateur;

class ControleurUtilisateur {
    private static function afficherVue(string $cheminVue, array $parametres = []): void {
        extract($parametres);
        require __DIR__ .  "/../vue/$cheminVue";
    }

    public static function afficherListe(): void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue('utilisateur/vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "liste.php"]);
    }

    // Affiche les détails d'un utilisateur
    public static function afficherDetail(): void
    {
        $login = $_GET["login"] ?? null;
        if ($login) {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);
            if ($utilisateur === null) {
                self::afficherVue('utilisateur/vueGenerale.php', ["titre" => "Erreur", "cheminCorpsVue" => "erreur.php"]);
            } else {
                self::afficherVue('utilisateur/vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "Détails d'un utilisateur", "cheminCorpsVue" => "detail.php"]);
            }
        } else {
            self::afficherVue('utilisateur/vueGenerale.php', ["titre" => "Erreur", "cheminCorpsVue" => "erreur.php"]);
        }
    }

    public static function afficherFormulaireCreation(): void {
        self::afficherVue('utilisateur/vueGenerale.php', ["titre" => "Formulaire de création", "cheminCorpsVue" => "formulaireCreation.php"]);
    }

    public static function creerDepuisFormulaire(): void {
        $login = $_GET["login"] ?? null;
        $last_name = $_GET["last_name"] ?? null;
        $first_name = $_GET["first_name"] ?? null;

        if ($login && $last_name && $first_name) {
            $utilisateur = new ModeleUtilisateur($login, $last_name, $first_name);
            $utilisateur->ajouter();

            $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();

            self::afficherVue('utilisateur/vueGenerale.php', ["titre" => "Utilisateur créé", "cheminCorpsVue" => "utilisateurCree.php", "utilisateurs" => $utilisateurs]);
        } else {
            self::afficherVue('utilisateur/vueGenerale.php', ["titre" => "Erreur", "cheminCorpsVue" => "erreur.php"]);
        }
    }

}
?>