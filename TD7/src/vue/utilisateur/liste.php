<?php
/** @var ModeleUtilisateur[] $utilisateurs */
foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());

    echo '<p>
                Utilisateur de login ' . $loginHTML .
                ' <a href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . $loginURL . '">(+d\'infos)</a>
                 - <a href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=utilisateur&action=supprimer&login='. $loginURL . '">Supprimer</a>
                 - <a href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login='. $loginURL . '">Modifier</a>
          </p>';
}
?>
<p>
    <a href="http://localhost/tds-php/TD7/web/controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation">Créer un utilisateur</a>
</p>