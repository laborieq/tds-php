<?php
namespace App\Covoiturage\Configuration;

class ConfigurationSite {
    public static function getURLAbsolue(): string {
        return "http://localhost/tds-php/TD8/web/controleurFrontal.php";
    }
}