<form method="get" action='controleurFrontal.php'>
    <input type='hidden' name='action' value='enregistrerPreference'>
    <fieldset>
        <?php
        use App\Covoiturage\Lib\PreferenceControleur;

        $preferenceActuelle = PreferenceControleur::lire();
        ?>
        <legend>Préférence :</legend>
        <p>
            <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"
                <?= $preferenceActuelle === 'utilisateur' ? 'checked' : '' ?>>
            <label for="utilisateurId">Utilisateur</label>

            <input type="radio" id="trajetId" name="controleur_defaut" value="trajet"
                <?= $preferenceActuelle === 'trajet' ? 'checked' : '' ?>>
            <label for="trajetId">Trajet</label>
        </p>
        <button type="submit">Enregistrer la préférence</button>
    </fieldset>
</form>