<?php
use App\Covoiturage\Modele\DataObject\Utilisateur;
/** @var Utilisateur $utilisateur */
/** @var bool $estAdmin */
$login = $utilisateur->getLogin();
$nom = $utilisateur->getNom();
$prenom = $utilisateur->getPrenom();
?>

<form method="get" action='controleurFrontal.php'>
    <input type='hidden' name='action' value='mettreAJour'>
    <input type="hidden" name='controleur' value='utilisateur'>
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login</label> :
            <?= "<input class='InputAddOn-field' type='text' value='$login' name='login' id='login_id' required readonly/>"?>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">Nom</label> :
            <?= "<input class='InputAddOn-field' type='text' value='$nom' name='nom' id='nom_id' required/>" ?>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">Prenom</label> :
            <?= "<input class='InputAddOn-field' type='text' value='$prenom' name='prenom' id='prenom_id' required/>" ?>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="old_mdpHache_id">Ancien mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="old_mdpHache" id="old_mdpHache_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="new_mdpHache_id">Nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="new_mdpHache" id="new_mdpHache_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="new_mdpHache2_id">Vérification du nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="new_mdpHache2" id="new_mdpHache2_id" required>
        </p>
        <?php if ($estAdmin): ?>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="estAdmin_id">Administrateur&#42;</label>
                <input class="InputAddOn-field" type="checkbox" name="estAdmin" id="estAdmin_id" <?= $utilisateur->isEstAdmin() ? 'checked' : '' ?>>
            </p>
        <?php endif; ?>
        <p class="InputAddOn">
            <input class="InputAddOn-field" type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>