<?php
namespace App\Covoiturage\Lib;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ConnexionUtilisateur {
    // L'utilisateur connecté sera enregistré en session associé à la clé suivante
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void {
        $session = Session::getInstance();
        $session->enregistrer(ConnexionUtilisateur::$cleConnexion, $loginUtilisateur);
    }

    public static function estConnecte(): bool {
        return Session::getInstance()->contient(ConnexionUtilisateur::$cleConnexion);
    }

    public static function deconnecter(): void {
        $session = Session::getInstance();
        $session->detruire();
    }

    public static function getLoginUtilisateurConnecte(): ?string {
        return Session::getInstance()->lire(ConnexionUtilisateur::$cleConnexion);
    }

    public static function estUtilisateur($login): bool {
        return self::estConnecte() && self::getLoginUtilisateurConnecte() == $login;
    }

    public static function estAdministrateur(): bool {
        if (!self::estConnecte()) {
            return false;
        }

        $login = self::getLoginUtilisateurConnecte();

        if ($login === null) {
            return false;
        }

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if ($utilisateur == null || !$utilisateur->isEstAdmin()) {
            return false;
        }

        return true;
    }
}