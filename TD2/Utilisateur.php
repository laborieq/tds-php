<?php
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    public function __construct(string $nom, string $prenom, string $login) {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->login = substr($login, 0, 64);
    }

    public function __toString() {
        return "Nom : $this->nom, Prénom : $this->prenom, Login : $this->login";
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur {
        return new Utilisateur(
            $utilisateurFormatTableau['nom'],
            $utilisateurFormatTableau['prenom'],
            $utilisateurFormatTableau['login']
        );
    }

    public static function recupererUtilisateurs(): array {
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $sql = "SELECT * FROM utilisateur";
        $pdoStatement = $pdo->query($sql);
        $utilisateurs = [];

        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }
}
?>