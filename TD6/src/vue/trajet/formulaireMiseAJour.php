<?php
/** @var Trajet $trajet */
$id = $trajet->getId();
$depart  = $trajet->getDepart();
$arrivee  = $trajet->getArrivee();
$date = $trajet->getDate()->format('Y-m-d');
$conducteur = $trajet->getConducteur()->getLogin();
$prix = $trajet->getPrix();
$fumeur = $trajet->isNonFumeur() ;
echo $fumeur;
if ($fumeur != ""){
    $checked = "checked";
}else{
    $checked = "";
}
?>

    <form method="get" action="controleurFrontal.php">
        <input type="hidden" name='action' value='mettreAJour'>
        <input type="hidden" name='controleur' value='trajet'>
        <fieldset>
            <legend>Mon formulaire :</legend>
            <?= "<input class='InputAddOn-field' type='hidden' value='$id' name='id' id='id_id' required readonly/>"?>
            <p class="InputAddOn">
                <label class="InputAddOn-item" for="depart_id">Depart</label> :
                <?= "<input class='InputAddOn-field' type='text' value='$depart' name='depart' id='depart_id' required/>"?>
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item"  for="arrivee_id">Arrivée</label> :
                <?= "<input class='InputAddOn-field' type='text' value='$arrivee' name='arrivee' id='arrivee_id' required/>"?>
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item"  for="date_id">Date</label> :
                <?= "<input class='InputAddOn-field' type='date' value='$date' name='date' id='date_id' required/>"?>
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item"  for="prix_id">Prix</label> :
                <?= "<input class='InputAddOn-field' type='number' value='$prix' name='prix' id='prix_id' required/>"?>
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item"  for="conducteurLogin_id">Login du conducteur</label> :
                <?= "<input class='InputAddOn-field' type='text' value='$conducteur' name='conducteurLogin' id='conducteurLogin_id' required/>"?>
            </p>
            <p class="InputAddOn">
                <label class="InputAddOn-item"  for="nonFumeur_id">Non Fumeur ?</label> :
                <?= "<input class='InputAddOn-field' type='checkbox' name='nonFumeur' id='nonFumeur_id' $checked/>"?>
            </p>
            <p class="InputAddOn">
                <input type="submit" value="Envoyer">
            </p>
        </fieldset>
    </form>