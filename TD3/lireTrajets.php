<?php
require_once 'ConnexionBaseDeDonnees.php';
require_once 'Trajet.php';

try {
    $trajets = Trajet::recupererTrajets();

    foreach ($trajets as $trajet) {
        echo $trajet . "<br>";

        $passagers = $trajet->getPassagers();

        if (!empty($passagers)) {
            echo "Passagers :<br>";
            foreach ($passagers as $passager) {
                echo "- " . $passager->getPrenom() . " " . $passager->getNom() . " (login : " . $passager->getLogin() . ")<br>";
            }
        } else {
            echo "Aucun passager pour ce trajet.<br>";
        }

        echo "<br>";
    }

} catch (PDOException $e) {
    echo "Erreur : " . $e->getMessage();
}
?>