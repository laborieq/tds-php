<?php

/** @var ModeleUtilisateur[] $utilisateurs */

use App\Covoiturage\Lib\ConnexionUtilisateur;

foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());

    echo '<p>
            Utilisateur de login ' . $loginHTML .
        ' <a href="http://localhost/tds-php/TD8/web/controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . $loginURL . '">(+d\'infos)</a>
         ';

    if (ConnexionUtilisateur::estAdministrateur()) {
        echo ' - <a href="http://localhost/tds-php/TD8/web/controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=' . $loginURL . '">Modifier</a>';
        //echo ' - <a href="http://localhost/tds-php/TD8/web/controleurFrontal.php?controleur=utilisateur&action=supprimer&login=' . $loginURL . '">Supprimer</a>';
    }

    echo '</p>';
}
?>

<p>
    <a href="http://localhost/tds-php/TD8/web/controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation">Créer un utilisateur</a>
</p>