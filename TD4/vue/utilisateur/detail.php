<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Liste des utilisateurs</title>
    </head>
    <body>
        <?php
        /** @var ModeleUtilisateur $utilisateur */
        echo '<p><strong>Login :</strong> ' . $utilisateur->getLogin() . '</p>';
        echo '<p><strong>Nom :</strong> ' . $utilisateur->getNom() . '</p>';
        echo '<p><strong>Prénom :</strong> ' . $utilisateur->getPrenom() . '</p>';
        ?>
    </body>
</html>