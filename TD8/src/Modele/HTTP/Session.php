<?php
namespace App\Covoiturage\Modele\HTTP;
use App\Covoiturage\Modele\HTTP\ConfigurationSite;
use Exception;

class Session {
    private static ?Session $instance = null;

    /**
     * @throws Exception
     */
    private function __construct() {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            if (session_start() === false) {
                throw new Exception("La session n'a pas réussi à démarrer.");
            }
        }
    }

    public static function getInstance(): Session {
        if (is_null(Session::$instance)) {
            Session::$instance = new Session();
            Session::$instance->verifierDerniereActivite();
        }
        return Session::$instance;
    }

    public function contient($nom): bool {
        return isset($_SESSION[$nom]);
    }

    public function enregistrer(string $nom, mixed $valeur): void {
        $_SESSION[$nom] = $valeur;
    }

    public function lire(string $nom): mixed {
        return $_SESSION[$nom] ?? null;
    }

    public function supprimer($nom): void {
        if ($this->contient($nom)) {
            unset($_SESSION[$nom]);
        }
    }

    public function detruire() : void {
        session_unset();     // unset $_SESSION variable for the run-time
        session_destroy();   // destroy session data in storage
        Cookie::supprimer(session_name()); // deletes the session cookie
        // Il faudra reconstruire la session au prochain appel de getInstance()
        Session::$instance = null;
    }

    private function verifierDerniereActivite(): void {
        $dureeExpiration = ConfigurationSite::getDureeExpirationSession();

        if (isset($_SESSION['derniere_activite'])) {
            if (time() - $_SESSION['derniere_activite'] > $dureeExpiration) {
                $this->detruire();
                throw new Exception("Session expirée en raison d'inactivité.");
            }
        }

        $_SESSION['derniere_activite'] = time();
    }
}