<?php
namespace App\Covoiturage\Modele\Repository;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Utilisateur;

class UtilisateurRepository extends AbstractRepository {
    protected function construireDepuisTableauSQL(array $objectFormatTableau) : Utilisateur {
        return new Utilisateur(
            $objectFormatTableau[0],
            $objectFormatTableau[1],
            $objectFormatTableau[2],
            $objectFormatTableau[3],
            $objectFormatTableau[4],
            $objectFormatTableau[5],
            $objectFormatTableau[6],
            $objectFormatTableau[7]
        );
    }

    protected function getNomTable(): string {
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string {
        return "login";
    }

    /** @return string[] */
    protected function getNomsColonnes() : array {
        return ["login", "nom", "prenom", "mdpHache", "estAdmin", "email", "emailAValider", "nonce"];
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array {
        /** @var Utilisateur $utilisateur */
        if ($utilisateur->isEstAdmin()){
            $estAdmin = 1;
        } else {
            $estAdmin = 0;
        }

        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
            "mdpHacheTag" => $utilisateur->getMdpHache(),
            "estAdminTag" => $estAdmin,
            "emailTag" => $utilisateur->getEmail(),
            "emailAValiderTag" => $utilisateur->getEmailAValider(),
            "nonceTag" => $utilisateur->getNonce()
        );
    }
}