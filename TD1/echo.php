<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
   
    <body>
        Voici le résultat du script PHP :
        <p>Liste des utilisateurs :</p>
        <?php
            $utilisateurs = [];
            $utilisateurs[1]['nom'] = 'Durif';
            $utilisateurs[1]['prenom'] = 'Sylvain';
            $utilisateurs[1]['login'] = 'lemonarquecosmiquedu72';

            $utilisateurs[2]['nom'] = 'Tichault';
            $utilisateurs[2]['prenom'] = 'Richard';
            $utilisateurs[2]['login'] = 'artich';

            if (empty($utilisateurs)) {
                echo "Il n’y a aucun utilisateur.";
            } else {
                foreach ($utilisateurs as $utilisateur) {
        ?>
            <ul>
                nom : <?php echo $utilisateur['nom'] ?> <br>
                prénom : <?php echo $utilisateur['prenom'] ?> <br>
                login : <?php echo $utilisateur['login'] ?> <br>
            </ul>
        <?php
                }
            }
        ?>

    </body>
</html> 