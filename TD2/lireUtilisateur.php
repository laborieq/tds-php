<?php
require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

try {
    $utilisateurs = Utilisateur::recupererUtilisateurs();

    foreach ($utilisateurs as $utilisateur) {
        echo $utilisateur . "<br>";
    }

} catch (PDOException $e) {
    echo "Erreur : " . $e->getMessage();
}
?>