<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title> Mon premier php </title>
    </head>
    <body>
        <form method="get" action="routeur.php">
            <fieldset>
                <legend>Mon formulaire :</legend>
                <p>
                    <label for="login_id">Login</label> :
                    <input type="text" placeholder="artich" name="login" id="login_id" required/>
                </p>
                <p>
                    <label for="last_name_id">Nom</label> :
                    <input type="text" placeholder="Tichault" name="last_name" id="last_name_id" required/>
                </p>
                <p>
                    <label for="first_name_id">Prénom</label> :
                    <input type="text" placeholder="Richard" name="first_name" id="first_name_id" required/>
                </p>
                <p>
                    <input type="submit" value="Envoyer" />
                </p>
            </fieldset>
            <input type='hidden' name='action' value='creerDepuisFormulaire'>
        </form>
    </body>
</html>