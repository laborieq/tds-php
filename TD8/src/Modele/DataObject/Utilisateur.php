<?php
namespace App\Covoiturage\Modele\DataObject;

class Utilisateur extends AbstractDataObject{

    private string $login;
    private string $nom;
    private string $prenom;
    private string $mdpHache;
    private bool $estAdmin;
    private string $email;
    private string $emailAValider;
    private string $nonce;

    public function getNom(): string {
        return $this->nom;
    }

    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    public function getPrenom(): string {
        return $this->prenom;
    }

    public function setPrenom(string $prenom) {
        $this->prenom = $prenom;
    }

    public function getLogin() {
        return $this->login;
    }

    public function setLogin(string $login) {
        $this->login = substr($login, 0, 64);
    }

    public function getMdpHache() {
        return $this->mdpHache;
    }

    public function setMdpHache() {
        $this->mdpHache = substr($this->mdpHache, 0, 256);
    }

    public function isEstAdmin(): bool {
        return $this->estAdmin;
    }

    public function setEstAdmin(bool $estAdmin): void {
        $this->estAdmin = $estAdmin;
    }

    public function getEmail(): string {
        return $this->email;
    }

    public function setEmail(string $email): void {
        $this->email = $email;
    }

    public function getEmailAValider(): string {
        return $this->emailAValider;
    }

    public function setEmailAValider(string $emailAValider): void {
        $this->emailAValider = $emailAValider;
    }

    public function getNonce(): string {
        return $this->nonce;
    }

    public function setNonce(string $nonce): void {
        $this->nonce = $nonce;
    }

    public function __construct( string $login, string $nom, string $prenom, string $mdpHache, bool $estAdmin, string $email, string $emailAValider, string $nonce) {
        $this->login = substr($login, 0, 64);
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->mdpHache = substr($mdpHache, 0, 256);
        $this->estAdmin = $estAdmin;
        $this->email = $email;
        $this->emailAValider = $emailAValider;
        $this->nonce = $nonce;
    }
}