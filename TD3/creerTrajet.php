<p>Le trajet partira de <?php echo $_POST['depart']; ?> et arrivera à <?php echo $_POST['arrivee']; ?>.</p>
<p>Il est prévu pour le <?php echo $_POST['date']; ?> et coûtera <?php echo $_POST['prix']; ?> euros.</p>
<p>
    <?php
    require_once 'Trajet.php';
    require_once 'Utilisateur.php';

    $conducteur = Utilisateur::recupererUtilisateurParLogin($_POST['loginConducteur']);

    if ($conducteur === null) {
        echo "Conducteur non trouvé.";
        exit();
    }

    $date = new DateTime($_POST['date']);

    $nonFumeur = isset($_POST['nonFumeur']) ? true : false;

    $trajet = new Trajet(
        null,
        $_POST['depart'],
        $_POST['arrivee'],
        $date,
        (int) $_POST['prix'],
        $conducteur,
        $nonFumeur
    );

    $trajet->ajouter();

    echo "Trajet ajouté avec succès !";
    ?>
</p>