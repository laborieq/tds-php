<?php
require_once ('../Modele/ModeleUtilisateur.php'); // Chargement du modèle

class ControleurUtilisateur {
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres);
        require "../vue/$cheminVue";
    }

    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs();
        self::afficherVue('utilisateur/liste.php', ['utilisateurs' => $utilisateurs]);
    }

    // Affiche les détails d'un utilisateur
    public static function afficherDetail() : void {
        $login = $_GET["login"] ?? null;
        if ($login) {
            $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);
            if ($utilisateur === null) {
                self::afficherVue('utilisateur/erreur.php');
            } else {
                self::afficherVue('utilisateur/detail.php', ['utilisateur' => $utilisateur]);
            }
        } else {
            self::afficherVue('utilisateur/erreur.php');
        }
    }

    public static function afficherFormulaireCreation() : void {
        self::afficherVue('utilisateur/formulaireCreation.php');
    }

    public static function creerDepuisFormulaire() : void {
        $login = $_GET["login"];
        $last_name = $_GET["last_name"];
        $first_name = $_GET["first_name"];

        $utilisateur = new ModeleUtilisateur($login, $last_name, $first_name);

        $utilisateur->ajouter();

        self::afficherListe();
    }
}
?>