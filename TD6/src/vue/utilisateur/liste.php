<?php
/** @var ModeleUtilisateur[] $utilisateurs */
foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());

    echo '<p>
                Utilisateur de login ' . $loginHTML .
                ' <a href="http://localhost/tds-php/TD6/web/controleurFrontal.php?action=afficherDetail&login=' . $loginURL . '">(+d\'infos)</a>
                 - <a href="http://localhost/tds-php/TD6/web/controleurFrontal.php?action=supprimer&login='. $loginURL . '">Supprimer</a>
                 - <a href="http://localhost/tds-php/TD6/web/controleurFrontal.php?action=afficherFormulaireMiseAJour&login='. $loginURL . '">Modifier</a>
          </p>';
}
?>
<p>
    <a href="http://localhost/tds-php/TD6/web/controleurFrontal.php?action=afficherFormulaireCreation">Créer un utilisateur</a>
</p>