<?php
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom() {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    // un getter
    public function getPrenom() {
        return $this->prenom;
    }

    // un setter
    public function setPrenom(string $prenom) {
        $this->prenom = $prenom;
    }

    // un getter
    public function getLogin() {
        return $this->login;
    }

    // un setter
    public function setLogin(string $login) {
        $this->login = $login;
    }

    // un constructeur
    public function __construct(
        string $nom,
        string $prenom,
        string $login
    ) {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->login = substr($login, 0, 64);
    }

    public function __toString() {
        return "Nom : $this->nom, prénom : $this->prenom, login : $this->login";
    }
}
?>