<?php
namespace App\TD7\Covoiturage\Controleur;
use App\TD7\Covoiturage\Modele\DataObject\Utilisateur;
use App\TD7\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique {
    public static function afficherListe() : void {
        //appel au modèle pour gérer la BD
        $utilisateurs = (new UtilisateurRepository)->recuperer();
        $titre = "Liste des utilisateurs";
        $cheminCorpsVue = "liste.php";
        ControleurGenerique::afficherVue(
            '../vue/utilisateur/vueGenerale.php',
            ["utilisateurs" => $utilisateurs, "titre" => $titre, "cheminCorpsVue" => $cheminCorpsVue]
        );
    }

    public static function afficherDetail() : void {
        $login = $_GET["login"];
        if ($login && (new UtilisateurRepository())->recupererParClePrimaire($login)) {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login); //appel au modèle pour gérer la BD
            ControleurGenerique::afficherVue(
                '../vue/utilisateur/vueGenerale.php',
                ["utilisateur" => $utilisateur, "titre" => "Détail utilisateur", "cheminCorpsVue" => "detail.php"]
            );
        }else {
            ControleurGenerique::afficherErreur("utilisateur inexistant");
        }
    }

    public static function afficherFormulaireCreation() : void {
        ControleurGenerique::afficherVue(
            '../vue/utilisateur/vueGenerale.php',
            ["titre" => "Creation utilisateur", "cheminCorpsVue" => "formulaireCreation.php"]
        );
    }

    public static function creerDepuisFormulaire() : void {
        $utilisateur = self::construireDepuisFormulaire(["login" => $_GET["login"], "nom" => $_GET["nom"], "prenom" => $_GET["prenom"]]);
        (new UtilisateurRepository)->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository)->recuperer();
        ControleurGenerique::afficherVue('../vue/utilisateur/vueGenerale.php', ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateurCree.php", "utilisateurs" => $utilisateurs]);
    }

    public static function supprimer() : void {
        if (isset($_GET["login"])) {
            $login = $_GET["login"];
            (new UtilisateurRepository)->supprimer($login);
            $utilisateurs = (new UtilisateurRepository)->recuperer();
            ControleurGenerique::afficherVue("../vue/utilisateur/vueGenerale.php", ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateurSupprime.php", "login" => $login, "utilisateurs" => $utilisateurs]);
        } else {
            ControleurGenerique::afficherErreur();
        }
    }
    public static function afficherFormulaireMiseAJour() : void {
        if (isset($_GET["login"])) {
            $login = $_GET["login"];
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
            ControleurGenerique::afficherVue("../vue/utilisateur/vueGenerale.php", ["titre" => "Formulaire de mise a jour", "cheminCorpsVue" => "formulaireMiseAJour.php", "utilisateur" => $utilisateur]);
        } else {
            ControleurGenerique::afficherErreur();
        }
    }
    public static function mettreAJour() : void{
        $login = $_GET["login"];
        if (isset($login)) {
            $temp = self::construireDepuisFormulaire(["login" => $_GET["login"], "nom" => $_GET["nom"], "prenom"=> $_GET["prenom"]]);
            (new UtilisateurRepository)->mettreAJour($temp);
            $utilisateurs = (new UtilisateurRepository)->recuperer();
            ControleurGenerique::afficherVue("../vue/utilisateur/vueGenerale.php", ["titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateurMisAJour.php", "utilisateurs" => $utilisateurs, "login" => $login]);
        } else {
            ControleurGenerique::afficherErreur();
        }
    }

    /**
     * @return Utilisateur
     */
    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur {
        return new Utilisateur($tableauDonneesFormulaire["login"], $tableauDonneesFormulaire["nom"], $tableauDonneesFormulaire["prenom"]);
    }
}
?>