<?php
namespace App\Covoiturage\Modele\HTTP;

class ConfigurationSite {
    static private int $dureeExpirationSession = 1800; //30 min.

    static public function getDureeExpirationSession(): int {
        return self::$dureeExpirationSession;
    }
}